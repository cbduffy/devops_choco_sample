#Get the packages root paths so we can use the bundled content.
[String]$PackageRoot = (Split-Path -Parent (Split-Path -Parent $MyInvocation.MyCommand.Definition))
[String]$Path = (Join-Path -Path $PackageRoot -ChildPath 'content')

 $arguments = @{}

  # Let's assume that the input string is something like this, and we will use a Regular Expression to parse the values
  # /Port:81 /Edition:LicenseKey /AdditionalTools

  # Now we can use the $env:chocolateyPackageParameters inside the Chocolatey package
  $packageParameters = $env:chocolateyPackageParameters

  # Default the values
  $DeployConfig = ""
  
  # Now parse the packageParameters using good old regular expression
  if ($packageParameters) {
      $match_pattern = "\/(?<option>([a-zA-Z]+)):(?<value>([`"'])?([a-zA-Z0-9- _\\:\.]+)([`"'])?)|\/(?<option>([a-zA-Z]+))"
      $option_name = 'option'
      $value_name = 'value'

      if ($packageParameters -match $match_pattern ){
          $results = $packageParameters | Select-String $match_pattern -AllMatches
          $results.matches | % {
            $arguments.Add(
                $_.Groups[$option_name].Value.Trim(),
                $_.Groups[$value_name].Value.Trim())
        }
      }
      else
      {
          Throw "Package Parameters were found but were invalid (REGEX Failure)"
      }

      if ($arguments.ContainsKey("DeployConfig")) {
          Write-Verbose -Message "DeployConfig Argument Found"
          $DeployConfig = $arguments["DeployConfig"]
      }
  } else {
      Write-Debug "No Package Parameters Passed in"
  }

  $silentArgs = "/S /Key:" + $port
  if ($additionalTools) { $silentArgs += " /Additionaltools" }

  Write-Debug "This would be the Chocolatey Silent Arguments: $silentArgs"

New-Item c:\choco_tmp -type directory

Copy-Item $Path\content\sample_choco.txt c:\choco_tmp
	
Write-Host " >>>>> END Choco Install <<<<<"