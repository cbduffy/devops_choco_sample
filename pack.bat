@echo off
:: Clean up and create directories
rd /q /s .git
mkdir tools
mkdir content

move chocolateyInstall.ps1 tools\
move sample_choco.txt content\

cd %WORKSPACE%

:: Make package
cpack sample.nuspec