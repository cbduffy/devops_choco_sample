### What is this repository for? ###

* Getting started with a basic chocolatey package.
* Version 1.0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* After cloning the repository make sure to follow the steps below:
* 1.) Setup nuspec file and choco install file that will be used to create your package.
* 2.) Define any dependencies inside your nuspec file. (Example below)
* 3.) Setup your install script for how you want the install to happen. (Example below with also how to pass it params.)
* 4.) Once script and nuspec file is ready run the pack commands to create the choco package itself. (Remember to move content files into correct locations during pack event)
* 5.) Choco package should be ready to go! Run the install commands to see the results.
	- Keep in mind that choco.exe has to be installed on a machine to be able to run the choco install command. 

### Contribution guidelines ###

* Make sure to be pointing where your package is stored when running install.
* If it works locally it should work on a server as well.
* Make sure to include dependencies to ensure consistency with app deployment.

### Who do I talk to? ###

* Chocolatey Public Website: https://chocolatey.org/
* GitHub: https://github.com/chocolatey/chocolatey
* Other community or team contact